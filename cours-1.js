window.onload = function (ev) {
     window.imageToMove = document.querySelector("#imageToMove");
     var resetPosBtn = document.querySelector("#reset");
     resetPosBtn.addEventListener("click", setInitialImage);
     window.modal = document.querySelector("#exampleModal");


     window.btns = document.querySelectorAll(".imageChoice");
     var buttonsChoice = window.btns;

     setInitialImage(imageToMove);

    function setInitialImage() {
        window.imageToMove.style.top = window.innerHeight / 2 + "px";
        window.imageToMove.style.left = window.innerWidth / 2 - 200 + "px";
    }

    function changePosition(distance, direction) {
        switch(direction) {
            case "left":
                window.imageToMove.style.left = distance + "px";
                break;
            case "top":
                window.imageToMove.style.top = distance + "px";
                break;
        }
    }

    document.addEventListener("keydown", function (ev2) {
        var key = ev2.key;
        console.log(key)
        if(key == "ArrowLeft") {
            var currentLeft = parseInt(window.imageToMove.style.left.replace("px", ""));
            if (currentLeft != 0) {
                changePosition(currentLeft - 20, "left");
            }
        } else if (key == "ArrowRight") {
            var currentRight = parseInt(window.imageToMove.style.left.replace("px", ""));
            if (currentRight != document.body.style.width) {
                changePosition(currentRight + 20, "left");
            }
        } else if (key == "ArrowUp") {
            var currentTop = parseInt(window.imageToMove.style.top.replace("px", ""));
            if (currentTop != 0) {
                changePosition(currentTop - 20, "top");
            }
        } else if (key == "ArrowDown") {
            var currentDown = parseInt(window.imageToMove.style.top.replace("px", ""));
            if (currentDown != 0) {
                changePosition(currentDown + 20, "top");
            }
        }
    })

    for(var i = 0; i < buttonsChoice.length; i++) {
        var b = buttonsChoice[i];
        b.addEventListener("click", function () {
            var image = this.dataset.image;
            window.imageToMove.src = image;
        });
    }

}